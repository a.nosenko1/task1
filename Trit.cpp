//
// Created by Vatson on 005 05.11.2020.
//

#include "Trit.h"
#include "TritSet.h"


Trit::Trit(int index, TritSet *set) {
    this->index = index;
    this->set = set;
}

TritValue Trit::getTritValue() const {
    if (index != -1)
        return  set->smartGetTritValue(index);
    else {
        std::cout << "trying to get a trit at index -1\n";
        return UNKNOWN;
    }
}

Trit &Trit::operator=(TritValue tv) { // должен вызвать просто установку трита
    set->smartSetTrit(index, tv);
    return *this;
}
Trit &Trit::operator=(const Trit &other) {
    if (this == &other) {
        return *this;
    }
    set->smartSetTrit(index, other.getTritValue());
    return *this;
}

std::ostream &operator<<(std::ostream &out, const Trit &t) {
    if (t.getTritValue() == TRUE)
        out << "TRUE";
    else if (t.getTritValue() == FALSE)
        out << "FALSE";
    else out << "UNKNOWN";
    return out;
}

bool Trit::operator==(TritValue tv) const {
    return tv == this->getTritValue();
}
bool Trit::operator==(const Trit &t) const {
    return t.getTritValue() == this->getTritValue();
}
bool operator==(TritValue tv, const Trit &t) {
    return tv == t.getTritValue();
}

TritValue Trit::operator!() const {
    if (this->getTritValue() == FALSE) { ;
        return TRUE;
    }
    if (this->getTritValue() == TRUE) {
        return FALSE;
    }
    return UNKNOWN;
}

TritValue Trit::operator&(const Trit &t) const {
    return t&this->getTritValue();
}
TritValue Trit::operator&(TritValue tv) const {
    if (tv > 1 || tv < -1){
        std::cout << "incorrect TritValue in &\n";
    }

    if (this->getTritValue() == TRUE) {
        if (tv == FALSE) {
            return FALSE;
        }
        if (tv == UNKNOWN) {
            return UNKNOWN;
        }
        if (tv == TRUE) {
            return TRUE;
        }
    }
    if (this->getTritValue() == UNKNOWN) {
        if (tv == FALSE) {
            return FALSE;
        }
        if (tv == UNKNOWN) {
            return UNKNOWN;
        }
        if (tv == TRUE) {
            return UNKNOWN;
        }
    }
    if (this->getTritValue() == FALSE) {
        return FALSE;
    }
    return UNKNOWN;
}
TritValue operator&(TritValue tv, const Trit &t) {
    return t&tv;
}

TritValue Trit::operator|(const Trit &t) const {
    return t|this->getTritValue();
}
TritValue Trit::operator|(TritValue tv) const {
    if (this->getTritValue() == FALSE) {
        if (tv == FALSE) {
            return FALSE;
        }
        if (tv == UNKNOWN) {
            return UNKNOWN;
        }
        if (tv == TRUE) {
            return TRUE;
        }
    }
    if (this->getTritValue() == UNKNOWN) {
        if (tv == FALSE) {
            return UNKNOWN;
        }
        if (tv == UNKNOWN) {
            return UNKNOWN;
        }
        if (tv == TRUE) {
            return TRUE;
        }
    }
    if (this->getTritValue() == TRUE) {
        return TRUE;
    }
    return UNKNOWN;
}
TritValue operator|(TritValue tv, const Trit &t) {
    return t|tv;
}

Trit::operator int() {
    return this->getTritValue();
}

Trit::Trit(int index, const TritSet *set) {
    this->index = index;
    this->set = const_cast<TritSet *>(set);
}




