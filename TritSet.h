//
// Created by Vatson on 005 05.11.2020.
//

#ifndef LAB1_TRY3_TRITSET_H
#include "includes.h"
class Trit;
class TritSet {
public:
    friend class Trit;
    explicit TritSet(uint trits_count);     // конструктор выделяет память под trits_count тритов
    explicit TritSet();
    explicit TritSet(TritSet *other);

    size_t capacity() const;                // размер в памяти

    void shrink();                          // освободить лишнюю память

    uint cardinality(TritValue tv) const;   // сколько таких тритов
    size_t length() const;                  // индекс последнего не Unknown трита +1
    void trim(size_t lastIndex);            // забыть содержимое от lastIndex и дальше

    const TritSet operator&(TritSet& ts) const; // чтобы избежать set|set1 = set2;
    const TritSet operator|(TritSet& ts) const;
    const TritSet operator!();

    TritSet& operator=(const TritSet& other);
    TritSet& operator&=(TritSet& other);
    TritSet& operator|=(TritSet& other);

    Trit& operator[](int index);

private:
    uint *trits;        // массив тритов
    uint alloc_size;    // сколько чистых байт надо на хранение, /sizeof + % для длины массива

    void smartSetTrit(int index, TritValue tv);
    Trit& smartGetTrit(int index) const;
    TritValue smartGetTritValue(int index);

    TritValue getTrit(uint trit_number) const;              // взять значение трита
    void setTrit(unsigned int trit_number, TritValue trit); // установить трит

    size_t capacity_inside() const;                // размер в памяти
    void capacityText() const;              // длина массива trits

    void takeMoreMemory(int index);         // выделить больше памяти
    bool isMore(int index);
    static bool whatBit(uint position, uint number);        // что за бит находится на такой позиции в этом number
    static TritValue twoBitsToOneTrit(bool first_bit, bool second_bit); // два бита в один трит
    uint arrayTritsLength() const;             // длина массива uint trits,
    uint howManyTritsLike(TritValue tv) const; // сколько таких тритов
    uint addressLastMeaningTrit() const;       // номер в массиве uint trits uint'a, после которого нет значащих
};


#endif //LAB1_TRY3_TRITSET_H
