#include <iostream>
#include "TritSet.h"
#include "Trit.h"
#include <gtest/gtest.h>

TEST(TritSet,capacity){
    TritSet set(1);
    size_t allocLenght = set.capacity();
    ASSERT_TRUE(allocLenght == 16);

    set[5000] = TRUE;
    allocLenght = set.capacity();
    ASSERT_TRUE(allocLenght == 16*(5000*2/8/uint_sizeof+(((5000*2/8)%uint_sizeof)?1:0)));

    set[6000] = UNKNOWN;
    size_t allocLenghtNew = set.capacity();
    ASSERT_TRUE(allocLenght == allocLenghtNew);

    TritSet set1(1000000000);
    size_t allocLenght1 = set1.capacity();
    ASSERT_TRUE(allocLenght1 >= 16*(1000000000*2/8/uint_sizeof+(((1000000000*2/8)%uint_sizeof)?1:0)));
    ASSERT_TRUE(allocLenght1 == 16*(1000000000*2/8/uint_sizeof+(((1000000000*2/8)%uint_sizeof)?1:0)+1));
}

TEST(TritSet, shrink){
    TritSet set(1000);
    size_t allocLenght = set.capacity();
    set[50] = TRUE;
    ASSERT_TRUE(allocLenght == set.capacity());
    set.shrink();
    ASSERT_TRUE(allocLenght > set.capacity());
    ASSERT_TRUE(set.capacity() == 16*(50 * 2 / 8 / uint_sizeof + (((50 * 2 / 8) % uint_sizeof) ? 1 : 0) + 1));
}

TEST(TritSet, length){ // индекс последнего не unknown
    TritSet set(1000);
    set[60] = TRUE;
    ASSERT_TRUE(set.length() == 60 +1);

    set[60] = UNKNOWN;
    ASSERT_TRUE(set.length() == 0 +1);

    TritSet set1(1000000000);
    set1[5000] = TRUE;
    ASSERT_TRUE(set1.length() == 5000 +1);
}

TEST(TritSet, trim){
    TritSet set(1000);
    set[50] = set[51] = TRUE;
    set.trim(51);
    ASSERT_TRUE(set[50] == TRUE);
    ASSERT_TRUE(set[51] == UNKNOWN);
}

TEST(TritSet, cardinality){
    TritSet set(1000);
    set[1] = set[2] = set[3] = TRUE;
    set[5] = set[6] = set[7] = set [8] = FALSE;
    ASSERT_TRUE(set.cardinality(TRUE) == 3);
    ASSERT_TRUE(set.cardinality(FALSE) == 4);
    ASSERT_EQ(set.cardinality(UNKNOWN), 1008 -8 -1); // т.к. кратно 16 и index на ед.меньше
}

TEST(TritSet, operators){
    TritSet set(100);
    set[0] = FALSE;
    set[1] = UNKNOWN;
    set[2] = TRUE;
    ASSERT_TRUE((set[0]&set[0]) == FALSE); // &
    ASSERT_TRUE((set[0]&set[1]) == FALSE);
    ASSERT_TRUE((set[1]&set[0]) == FALSE);
    ASSERT_TRUE((set[1]&set[2]) == UNKNOWN);
    ASSERT_TRUE((set[2]&set[1]) == UNKNOWN);
    ASSERT_TRUE((set[1]&set[1]) == UNKNOWN);
    ASSERT_TRUE((set[2]&set[2]) == TRUE);
    ASSERT_TRUE((set[2]&set[0]) == FALSE);
    ASSERT_TRUE((set[0]&set[2]) == FALSE);

    ASSERT_TRUE((set[0]|set[0]) == FALSE);  // |
    ASSERT_TRUE((set[0]|set[1]) == UNKNOWN);
    ASSERT_TRUE((set[1]|set[0]) == UNKNOWN);
    ASSERT_TRUE((set[1]|set[2]) == TRUE);
    ASSERT_TRUE((set[2]|set[1]) == TRUE);
    ASSERT_TRUE((set[1]|set[1]) == UNKNOWN);
    ASSERT_TRUE((set[2]|set[2]) == TRUE);
    ASSERT_TRUE((set[2]|set[0]) == TRUE);
    ASSERT_TRUE((set[0]|set[2]) == TRUE);

    TritSet setA(1000);  // &=
    setA[5] = FALSE;
    setA[6] = TRUE;
    setA[7] = UNKNOWN;
    TritSet setB(2000);
    setB[5] = TRUE;
    setB[6] = TRUE;
    setB[7] = TRUE;
    setB[1500] = TRUE;
    setB[1501] = FALSE;
    setB[1502] = UNKNOWN;
    setA &= setB;
    ASSERT_TRUE(setA.capacity() == setB.capacity());
    ASSERT_TRUE(setA[5] == FALSE);
    ASSERT_TRUE(setA[6] == TRUE);
    ASSERT_TRUE(setA[7] == UNKNOWN);
    ASSERT_TRUE(setA[1500] == TRUE);
    ASSERT_TRUE(setA[1501] == FALSE);
    ASSERT_TRUE(setA[1502] == UNKNOWN);

    ASSERT_FALSE(setA[3000] == TRUE);

    TritSet setA1(1000);  // |=
    setA1[5] = FALSE;
    setA1[6] = TRUE;
    setA1[7] = UNKNOWN;
    TritSet setB1(2000);
    setB1[5] = TRUE;
    setB1[6] = TRUE;
    setB1[7] = TRUE;
    setB1[1500] = TRUE;
    setB1[1501] = FALSE;
    setB1[1502] = UNKNOWN;
    setA1 |= setB1;
    ASSERT_TRUE(setA1.capacity() == setB1.capacity());
    ASSERT_TRUE(setA1[5] == TRUE);
    ASSERT_TRUE(setA1[6] == TRUE);
    ASSERT_TRUE(setA1[7] == TRUE);
    ASSERT_TRUE(setA1[1500] == TRUE);
    ASSERT_TRUE(setA1[1501] == FALSE);
    ASSERT_TRUE(setA1[1502] == UNKNOWN);

    TritSet setC(1000);  // !
    setC[0] = TRUE;
    setC[1] = UNKNOWN;
    setC[2] = FALSE;
    TritSet setNoSetC = !setC;
    ASSERT_TRUE(setNoSetC[0] == FALSE);
    ASSERT_TRUE(setNoSetC[1] == UNKNOWN);
    ASSERT_TRUE(setNoSetC[2] == TRUE);

}


int main(int argc, char **argv) {

//    TritSet set2 = set1; // ЗНАЧЕНИЕ! какой оператор? коп или присв.
//    TritSet set2;
//    set2 = set1; // что то изменится?
//
//    set1[1] = FALSE & set1[3];
//    std::cout << set1[1] ;
//    set1[1] = TRUE & set1[3];
//    set1[1] = set1[2] & TRUE;
    TritSet set(2);
    TritSet set2(15);
    set[0] = TRUE;
    set2[1] = FALSE;
    TritSet set3(18);
    set3 = set.operator|(set2); // set.operator|(set2) const

    for (int i = 0; i < set3.capacity(); ++i) {
        std::cout << set3[i] << '\n';
    }
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
//    return 0;
}
