//
// Created by Vatson on 005 05.11.2020.
//

#ifndef LAB1_TRY3_TRIT_H
#define LAB1_TRY3_TRIT_H
#include "includes.h"
class TritSet;

class Trit {
public:

    Trit(int index, TritSet* set); // без TritValue
    Trit(int index, const TritSet* set); // без TritValue

    TritValue getTritValue() const;

    Trit& operator=(TritValue tv);
    Trit& operator=(const Trit& other);

    operator int();

    friend std::ostream& operator<< (std::ostream &out, const Trit &t);

    bool operator==(const Trit& t) const;
    bool operator==(TritValue tv) const; // такие
    friend bool operator==(TritValue tv, const Trit& t);

    TritValue operator&(const Trit& t) const;
    TritValue operator&(TritValue tv) const; // только этот реализован
    friend TritValue operator&(TritValue tv, const Trit& t);

    TritValue operator|(const Trit& t) const;
    TritValue operator|(TritValue tv) const; // только этот реализован
    friend TritValue operator|(TritValue tv, const Trit& t);

    TritValue operator!() const;

private:
    TritSet* set = nullptr;
    int index = -1;

};


#endif //LAB1_TRY3_TRIT_H
