//
// Created by Vatson on 005 05.11.2020.
//

#include "TritSet.h"
#include "Trit.h"

TritSet::TritSet(uint trits_count) {
    this->alloc_size = trits_count/4 + 1; // т.к. в 1 байте 4 трита
    trits = new uint[arrayTritsLength()];
    for (int i = 0; i < arrayTritsLength(); ++i) {
        trits[i] = 2863311530; // сомнительно, но быстро :)
//        for (int j = 0; j < uint_sizeof*8; j+=2) {
//            trits[i] = std::bitset<sizeof(unsigned int) * CHAR_BIT>(trits[i]).set(j,false).to_ulong();
//        }
//        for (int j = 1; j < uint_sizeof*8; j+=2) {
//            trits[i] = std::bitset<sizeof(unsigned int) * CHAR_BIT>(trits[i]).set(j,true).to_ulong();
//        }
    }
}

TritSet::TritSet(TritSet *other) {
    this->alloc_size = other->alloc_size;
    trits = new uint[this->arrayTritsLength()];
    for (int i = 0; i < arrayTritsLength(); ++i) {
        this->trits[i] = other->trits[i];
    }
}
TritSet::TritSet() {
    this->alloc_size = 0;
    this->trits = nullptr;
}
size_t TritSet::capacity_inside() const {
    return this->arrayTritsLength();
}
void TritSet::capacityText() const {
    std::cout << "Array length: " << arrayTritsLength() << " elements\n";
    std::cout << "Clear size: " << alloc_size << " bytes\n";
    std::cout << "Array weight: " << arrayTritsLength()*uint_sizeof << " bytes\n";
}

void TritSet::shrink() {
    uint alloc_size_new = addressLastMeaningTrit() / 4 + 1;
    uint* trits1 = new uint[alloc_size_new/uint_sizeof+(alloc_size_new%uint_sizeof?1:0)];
    for (int i = 0; i < alloc_size_new/uint_sizeof+(alloc_size_new%uint_sizeof?1:0); ++i) {
        trits1[i] = trits[i];
    }
    delete trits;
    trits = trits1;
    alloc_size = alloc_size_new;
}
uint TritSet::addressLastMeaningTrit() const {
    for (uint i = arrayTritsLength()*uint_sizeof*8/2-1; i > 0; i--) {
        if (getTrit(i) != UNKNOWN)
            return i;
    }
    return 0;
}
void TritSet::takeMoreMemory(int index) { // index > чем старый обязательно
    int alloc_size_new = index/4 + 1;
    uint* trits1 = new uint[alloc_size_new/uint_sizeof+(alloc_size_new%uint_sizeof?1:0)];
    for (int i = 0; i < alloc_size/uint_sizeof; ++i) {
        trits1[i] = trits[i];
    }
    delete trits;
    trits = trits1;
    alloc_size = alloc_size_new;
}

bool TritSet::whatBit(unsigned int position, unsigned int number) {
    return std::bitset<sizeof(unsigned int) * CHAR_BIT>(number).test(sizeof(unsigned int)*8 - position - 1); // (number >> (sizeof(unsigned int)*8 - position - 1)) & 1;
}
TritValue TritSet::twoBitsToOneTrit(bool first_bit, bool second_bit) {
    if (first_bit == 1 && second_bit == 1)
        return TRUE;
    else if (first_bit == 0 && second_bit == 0)
        return FALSE;
    else return UNKNOWN;
}
TritValue TritSet::getTrit(uint trit_number) const {
    uint byte_number = trit_number/4; // какой байт
    uint array_position = byte_number/uint_sizeof;
    uint e16trits = this->trits[array_position];
    if (e16trits == 2863311530) // сомнительно, но быстро :)
        return UNKNOWN;
    uint first_position = (trit_number - 4*uint_sizeof*(array_position))*2;
    return twoBitsToOneTrit(whatBit(first_position, e16trits), whatBit(first_position+1, e16trits));
}
void TritSet::setTrit(unsigned int trit_number, TritValue trit) {
    uint byte_number = trit_number/4; // байт в uint
    uint array_position = byte_number/uint_sizeof;
    uint first_position = (trit_number - 4*uint_sizeof*(array_position))*2;
    if (trit == TRUE) {
        trits[array_position] = std::bitset<uint_sizeof * CHAR_BIT>(trits[array_position]).set(
                uint_sizeof * 8 - first_position - 1, true).to_ulong();
        trits[array_position] = std::bitset<uint_sizeof * CHAR_BIT>(trits[array_position]).set(
                uint_sizeof * 8 - first_position - 2, true).to_ulong();
    } else {
        trits[array_position] = std::bitset<uint_sizeof * CHAR_BIT>(trits[array_position]).set(
                uint_sizeof * 8 - first_position - 2, false).to_ulong();
        trits[array_position] = std::bitset<uint_sizeof * CHAR_BIT>(trits[array_position]).set(
                uint_sizeof * 8 - first_position - 1, !(trit == FALSE)).to_ulong();
    }
}

Trit& TritSet::operator[](int index) {
    Trit* t = new Trit(index, this);
    return *t;
}

void TritSet::smartSetTrit(int index, TritValue tv) {
    if (index == -1){
        std::cout << "trying to set a trit at index -1\n";
        return;
    }
    if (isMore(index) && tv != UNKNOWN){
        this->takeMoreMemory(index);
        this->setTrit(index, tv);
    }
    else if (!isMore(index))
        this->setTrit(index, tv);
}

uint TritSet::arrayTritsLength() const{
    return alloc_size/uint_sizeof+(alloc_size%uint_sizeof?1:0);
}

const TritSet TritSet::operator&(TritSet &ts) const{
    if (this->capacity_inside() >= ts.capacity_inside()) {
        auto* new_set = new TritSet(*this);
        for (int i = 0; i < ts.capacity_inside() * uint_sizeof * 8 / 2 - 1; ++i) {
            new_set->setTrit(i,( this->smartGetTrit(i) & ts.smartGetTrit(i) ));
        }
        for (uint j = ts.capacity_inside() * uint_sizeof * 8 / 2 - 1; j < this->capacity_inside() * uint_sizeof * 8 / 2 - 1; ++j) {
            new_set->setTrit(j,this->getTrit(j));
        }
        return *new_set;
    } else {
        auto* new_set = new TritSet(ts);
        for (int i = 0; i < this->capacity_inside() * uint_sizeof * 8 / 2 - 1; ++i) {
            new_set->setTrit(i,( this->smartGetTrit(i) & ts.smartGetTrit(i) ));
        }
        for (uint j = this->capacity_inside() * uint_sizeof * 8 / 2 - 1; j < ts.capacity_inside() * uint_sizeof * 8 / 2 - 1; ++j) {
            new_set->setTrit(j,ts.getTrit(j));
        }
        return *new_set;
    }
}

const TritSet TritSet::operator|(TritSet &ts) const{
    if (this->capacity_inside() >= ts.capacity_inside()) {
        auto* new_set = new TritSet(*this);
        for (int i = 0; i < ts.capacity_inside() * uint_sizeof * 8 / 2 - 1; ++i) {
            new_set->setTrit(i,( this->smartGetTrit(i) | ts.smartGetTrit(i) ));
        }
        for (uint j = ts.capacity_inside() * uint_sizeof * 8 / 2 - 1; j < this->capacity_inside() * uint_sizeof * 8 / 2 - 1; ++j) {
            new_set->setTrit(j,this->getTrit(j));
        }
        return *new_set;
    } else {
        auto* new_set = new TritSet(ts);
        for (int i = 0; i < this->capacity_inside() * uint_sizeof * 8 / 2 - 1; ++i) {
            new_set->setTrit(i,( this->smartGetTrit(i) | ts.smartGetTrit(i) ));
        }
        for (uint j = this->capacity_inside() * uint_sizeof * 8 / 2 - 1; j < ts.capacity_inside() * uint_sizeof * 8 / 2 - 1; ++j) {
            new_set->setTrit(j, ts.getTrit(j));
        }
        return *new_set;
    }
}

TritSet &TritSet::operator=(const TritSet &other) {
    if (this == &other) {
        other.trits[1] = FALSE;
        return *this;
    }
    this->alloc_size = other.alloc_size;
    delete this->trits;
    trits = new uint[this->arrayTritsLength()];
    for (int i = 0; i < arrayTritsLength(); ++i) {
        this->trits[i] = other.trits[i];
    }
    return *this;
}

TritSet &TritSet::operator&=(TritSet &other) {
    *this = (*this & other);
    return *this;
}

TritSet &TritSet::operator|=(TritSet &other) {
    *this = (*this|other);
    return *this;
}

uint TritSet::howManyTritsLike(TritValue tv) const {
    if (tv == UNKNOWN){
        return arrayTritsLength()*uint_sizeof*8/2 - 1 - addressLastMeaningTrit();
    }
    uint count_trits = 0;
    for (uint i = arrayTritsLength()*uint_sizeof*8/2-1; i > 0; i--) {
        if (getTrit(i) == tv)
            count_trits++;
    }
    return count_trits;
}

uint TritSet::cardinality(TritValue tv) const {
    return howManyTritsLike(tv);
}

size_t TritSet::length() const {
    return addressLastMeaningTrit() + 1;
}

void TritSet::trim(size_t lastIndex) {
    for (int i = lastIndex; i < lastIndex+uint_sizeof*4; ++i) {
        this->setTrit(i,UNKNOWN);
    }
    uint byte_number = lastIndex/4; // какой байт
    uint array_position = byte_number/uint_sizeof+1;
    for (int i = array_position; i < arrayTritsLength(); ++i) {
        trits[i] = 2863311530;
    }
}

Trit& TritSet::smartGetTrit(int index) const{
    Trit* new_trit = new Trit(index, this);
    return *new_trit;
}

const TritSet TritSet::operator!() {
    auto* new_set = new TritSet(*this);
    for (int i = 0; i < this->capacity_inside() * uint_sizeof * 8 / 2 - 1; ++i) {
        new_set->setTrit(i,( !this->smartGetTrit(i) ));
    }
    return *new_set;
}

TritValue TritSet::smartGetTritValue(int index) {
    if (isMore(index)){
        return UNKNOWN;
    }
    return this->getTrit(index);
}

bool TritSet::isMore(int index) {
    return index + 1 > alloc_size * uint_sizeof;
}

size_t TritSet::capacity() const {
    return capacity_inside()*16;
}














